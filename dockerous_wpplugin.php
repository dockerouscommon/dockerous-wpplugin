<?php
/**
 * Plugin Name: Dockerous WPPlugin
 * Description: Wordpress Plugin containing base classes for Dockerous Plugins and Themes
 * Version:0.1
 * Author: Dockerous
 * License: GPLV3
 * License URI: https://www.gnu.org/licenses/gpl.html
 */


define("DOCKEROUS_WPPLUGIN_VERSION", "0.1");
define("DOCKEROUS_WPPLUGIN_NAME", "Dockerous WP Base");
if(!class_exists("DockerousActivationManager")){
    require_once 'activate.php';
}

if(!class_exists('DockerousWPPlugin')){
    class DockerousWPPlugin{
        public $activate;
        public $config;
        
        function __construct() {
            global $dockerous_plugins;
            if(!is_array($dockerous_plugins)){
                $dockerous_plugins = array();
            }
            $this->config = require 'config.php';
            $this->activate = new DockerousActivationManager(__FILE__,plugin_basename( __FILE__ ),$this->config);
            add_action('plugins_loaded', array($this, 'load'), 15);
        }
        
        function load(){
            require_once 'autoloader.php';
        }
    }
}
$dockerous_wpplugin = new DockerousWPPlugin();