<?php

return array(
    "plugin_name" => DOCKEROUS_WPPLUGIN_NAME,
    "required" => array("php","dwp"),
    "dwp_version" => DOCKEROUS_WPPLUGIN_VERSION,
    "dwp_version_check" => ">=",
    "mysql_version" => "5.5",
    "mysql_version_check" => ">=",
    "php_version" => "5.4",
    "php_version_check" => ">=",
    "dwp_prefix" => "dwp"
);