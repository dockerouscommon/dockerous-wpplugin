<?php

class DockerousAutoloader{
    
    
    function __construct() {
        spl_autoload_register(array($this, 'loader'));
    }
    
    function loader($class_name){
        if ( false === strpos( $class_name, 'Dockerous' ) ) {
            return;
        }
        $file_path = '';
        $file_parts = explode( '\\', $class_name );
        $plugin_loc = "/";
        for($i = 0; $i < count($file_parts); $i++){
            $current = strtolower( $file_parts[ $i ] );
            $current = str_ireplace( '_', '-', $current );
            if ( count( $file_parts ) - 1 === $i ) {
                $file_name = "/".$current.".php";
            }elseif($i == 0){
                $plugin_loc .= $current;
            }elseif($i == 1){
                $plugin_loc .= "-".$current;
            }else{
                $file_path .= '/' . $current;
            }
        }
        // If the file exists in the specified path, then include it.
        if(file_exists(WPMU_PLUGIN_DIR.$plugin_loc.$file_path.$file_name)){
            include_once( WPMU_PLUGIN_DIR.$plugin_loc.$file_path.$file_name );
        }elseif (WP_PLUGIN_DIR.$plugin_loc.$file_path.$file_name) {
            include_once( WP_PLUGIN_DIR.$plugin_loc.$file_path.$file_name );
        }else{
            global $dockerous_plugins;
            if(is_array($dockerous_plugins)){
                foreach($dockerous_plugins as $location){
                    if ( file_exists( $location.$file_path.$file_name ) ) {
                        include_once( $location.$file_path.$file_name );
                        break;
                    }
                }
            }
        }
    }
}