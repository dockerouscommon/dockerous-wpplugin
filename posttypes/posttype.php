<?php

namespace Dockerous\WPPlugin\PostTypes;

abstract class PostType {

    /** Capability type to use when registering the custom post type. */
    private $post_capability_type;

    /** Name to use when registering the custom post type. */
    private $post_type_name;

    /** Label to identify post type */
    private $post_type_label;
    private $label = '';

    function __construct($options) {
        $this->options = $options;
        if (!empty($this->label)) {
            add_action('init', array($this, 'init'));
            add_action('load-post.php', array($this, 'meta_boxes_setup'));
            add_action('load-post-new.php', array($this, 'meta_boxes_setup'));
        }
    }

    final function get_post_capability_type() {
        return $this->post_capability_type;
    }

    final function get_post_type_name() {
        return $this->post_type_name;
    }

    final function get_post_type_label() {
        return $this->post_type_label;
    }

    final protected function set_label($label) {
        $this->label = $label;
    }

    final protected function set_post_capability_type($new_val = 'post') {
        $this->post_capability_type = apply_filters($this->label . '_post_capability_type', $new_val);
    }

    final protected function set_post_type_name($new_val = '') {
        if ($new_val == '') {
            $new_val = $this->label;
        }
        $this->post_type_name = apply_filters($this->label . '_post_type_name', $new_val);
    }

    final protected function set_post_type_label($new_val = '') {
        if ($new_val == '') {
            $new_val = ucfirst($this->label);
        }
        $this->post_type_label = apply_filters($this->label . '_post_type_label', $new_val);
    }

    final protected function init(){
        $this->start_init();
        $labels = $this->post_type_labels();
        $args = $this->post_type_args();
        register_post_type($this->get_post_type_name(), $args);
    }
    
    protected function start_init() {
        $this->set_post_type_name();
        $this->set_post_capability_type();
        $this->set_post_type_label();
    }

    protected function post_type_labels(){
        return array(
            'name' => _x($this->get_post_type_label() . 's', 'post type general name'),
            'singular_name' => _x($this->get_post_type_label(), 'post type singular name'),
            'add_new' => _x('Add New', $this->get_post_type_label()),
            'add_new_item' => __('Add New ' . $this->get_post_type_label()),
            'edit_item' => __('Edit ' . $this->get_post_type_label()),
            'new_item' => __('New ' . $this->get_post_type_label()),
            'view_item' => __('View ' . $this->get_post_type_label()),
            'search_items' => __('Search ' . $this->get_post_type_label()),
            'not_found' => __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in Trash'),
            'parent_item_colon' => ''
        );
    }
    
    protected function post_type_args(){
        return array(
            'label' => $this->get_post_type_label(),
            'labels' => $labels,
            'supports' => array('title'),
            'hierarchical' => false,
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => false,
            'show_in_nav_menus' => false,
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'rewrite' => false,
            'capability_type' => 'page',
        );
    }

    final function output_input($object, $key, $subtext = '', $size = NULL, $disabled = FALSE) {
        $k = $this->options->prefix . '_' . $key;
        $value = get_post_meta($object->ID, $key, true);
        $size_str = '';
        if (isset($size) && is_int($size)) {
            $size_str = ' size="' . $size . '"';
        }
        $d = '';
        if ($disabled) {
            $d = ' disabled="disabled" ';
        }
        echo '<input type="text" name="' . $k . '" id="' . $k . '" value="' . $value . '" ' . $size_str . $d . '/>';
        echo $subtext;
    }

    final function output_checkbox($object, $key, $subtext = '', $disabled = FALSE) {
        $k = $this->page_id;
        $value = get_post_meta($object->ID, $key, true);
        $d = '';
        if ($disabled) {
            $d = ' disabled="disabled" ';
        }
        $checked = checked($value, 1, FALSE);
        echo '<input type="checkbox" name="' . $k . '" id="' . $k . '" value="1" ' . $d . $checked . '/>';
        echo $subtext;
    }

    final function output_hidden($object, $key, $subtext = '') {
        $k = $this->page_id;
        $value = get_post_meta($object->ID, $key, true);
        echo '<input type="hidden" name="' . $k . '" id="' . $k . '" value="' . $value . '" />';
        echo $subtext;
    }

    function meta_boxes_add() {

    }

    function meta_boxes_setup() {
        add_action('add_meta_boxes', array($this, 'meta_boxes_add'));
        add_action('save_post_' . $this->get_post_type_name(), array($this, 'save_post'), 10, 2);
        add_action('pre_post_update', array($this, 'save_post'), 10, 2);
    }

    function save_post($post_id, $post, $update) {
        return $this->valid_save($post_id, $post);
    }

    private function valid_save($post_id, $post) {
        $post = get_post($post_id);
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return FALSE;
        }
        if ($post->post_type != $this->get_post_type_name()) {
            return FALSE;
        }
        $nonce = filter_input(INPUT_POST, $this->get_post_type_name() . '_nonce');
        if (empty($nonce) || !wp_verify_nonce($nonce, $this->get_post_type_name() . '_nonce_action')) {
            return FALSE;
        }

        $post_type = get_post_type_object($post->post_type);
        if (!current_user_can($post_type->cap->edit_post, $post_id)) {
            return FALSE;
        }
        return TRUE;
    }

    protected function update_meta($post_id, $meta_key, $new_value) {
        $post = get_post($post_id, OBJECT);
        if (isset($post)) {
            $old_value = get_post_meta($post_id, $meta_key, true);
            if ($new_value && empty($old_value)) {
                return add_post_meta($post_id, $meta_key, $new_value, true);
            } elseif (current_user_can('manage_options')) {
                if (empty($new_value)) {
                    delete_post_meta($post_id, $meta_key, $old_value);
                    return NULL;
                } elseif ($new_value && $new_value != $old_value) {
                    delete_post_meta($post_id, $meta_key, $old_value);
                    return update_post_meta($post_id, $meta_key, $new_value);
                }
            }
        } else {
            return FALSE;
        }
    }

}

