<?php

namespace Dockerous\WPPlugin\Admin;

abstract class Menu {

    private $screen_id;
    private $page_id;
    private $prefix;
    private $options;
    private $menu_title;
    private $submenu_title = '';
    private $page_title;
    private $page_headline;
    private $top_menu_id = '';
    private $capability = 'manage_options';
    private $icon_url = '';
    private $menu_location = '';

    function __construct($options, $toplevel = '') {
        $this->options = $options;
        $this->prefix = $this->options->prefix;
        $this->set_screen_id($toplevel);
        add_filter($this->options->option_key, array($this, 'add_option'));
        add_action('admin_init', array($this, 'register_settings'));
        add_action('admin_menu', array($this, 'admin_menu'));
        //Put in Child Class
//        $this->page_id = $options->prefix . '_some_tag';
//        $this->menu_title = "Some Menu";//Appears in the menu in the admin dashboard
//        $this->page_title = "Amazon Redirect Settings";//Appears in the title bar at the top of the page
//        $this->page_headline = "Amazon Redirect Settings";//Appears at the top of the menu page
//        $this->submenu_title = "Some Sub Menu";//[Optional] Only use if you want a submennu that goes to the same as your main menu
//        $this->icon_url = '';//[Optional] Adds a custom icon to the main menu 
//        $this->menu_location = '';//[Optional] sets the location of the main menu
    }
    
    final function menu_id(){
        return $this->page_id;
    }

    final function add_option($a) {
        if (is_array($a)) {
            $a[] = $this->page_id;
        }
        return $a;
    }

    final function set_screen_id($toplevel = '') {
        if (empty($toplevel)) {
            $this->screen_id = $this->prefix . '_page_' . $this->page_id;
        } else {
            $this->screen_id = 'toplevel_page_' . $this->page_id;
            $this->top_menu_id = $toplevel;
        }
        if(empty($this->submenu_title)){
            $this->submenu_title = $this->menu_title;
        }
    }

    /*
     * Actions to be taken prior to page loading. This is after headers have been set.
     * @uses load-$hook
     */

    final function add_screen_meta_boxes() {
        /* Trigger the add_meta_boxes hooks to allow meta boxes to be added */
        do_action('add_meta_boxes_' . $this->screen_id, null);
        do_action('add_meta_boxes', $this->screen_id, null);

        /* Enqueue WordPress' script for handling the meta boxes */
        wp_enqueue_script('postbox');

        /* Add screen option: user can choose between 1 or 2 columns (default 2) */
        add_screen_option('layout_columns', array('max' => 2, 'default' => 2));
    }

    final function output_input($key, $subtext = '', $size = NULL, $disabled = FALSE) {
        $k = $this->page_id;
        $value = $this->options->$key;
        $size_str = '';
        if (isset($size) && is_int($size)) {
            $size_str = ' size="' . $size . '"';
        }
        $d = '';
        if ($disabled) {
            $d = ' disabled="disabled" ';
        }
        echo '<input type="text" name="' . $k . '[' . $key . ']" id="' . $k . '[' . $key . ']" value="' . $value . '" ' . $size_str . $d . '/>';
        echo $subtext;
    }

    final function output_checkbox($key, $subtext = '', $disabled = FALSE) {
        $k = $this->page_id;
        $value = $this->options->$key;
        $d = '';
        if ($disabled) {
            $d = ' disabled="disabled" ';
        }
        $checked = checked($value, 1, FALSE);
        echo '<input type="checkbox" name="' . $k . '[' . $key . ']" id="' . $k . '[' . $key . ']" value="1" ' . $d . $checked . '/>';
        echo $subtext;
    }

    final function output_hidden($key, $subtext = '') {
        $k = $this->page_id;
        $value = $this->options->$key;
        echo '<input type="hidden" name="' . $k . '[' . $key . ']" id="' . $k . '[' . $key . ']" value="' . $value . '" />';
        echo $subtext;
    }

    final function admin_menu(){
        if(empty($this->top_menu_id)){
            add_menu_page(
                $this->page_title,          // The title to be displayed on the corresponding page for this menu
                $this->menu_title,                  // The text to be displayed for this actual menu item
                $this->capability,            // Which type of users can see this menu
                $this->page_id,                  // The unique ID - that is, the slug - for this menu item
                array($this, 'settings_page'),// The name of the function to call when rendering the menu for this page
                $this->icon_url,
                $this->menu_location
            );
            if(!empty($this->submenu_title)){
                add_submenu_page(
                    $this->top_menu_id,                  // Register this submenu with the menu defined above
                    $this->page_title,          // The text to the display in the browser when this menu item is active
                    $this->submenu_title,                  // The text for this menu item
                    $this->capability,            // Which type of users can see this menu
                    $this->page_id,          // The unique ID - the slug - for this menu item
                    array($this, 'settings_page')   // The function used to render the menu for this page to the screen
                );
            }
        }else{
            add_submenu_page(
                $this->top_menu_id,                  // Register this submenu with the menu defined above
                $this->page_title,          // The text to the display in the browser when this menu item is active
                $this->submenu_title,                  // The text for this menu item
                $this->capability,            // Which type of users can see this menu
                $this->page_id,          // The unique ID - the slug - for this menu item
                array($this, 'settings_page')   // The function used to render the menu for this page to the screen
            );
        }
    }

    function open($header) {
        ?>
        <!-- Create a header in the default WordPress 'wrap' container -->
        <div class="wrap">
            <?php screen_icon() ?>
            <?php
            if (isset($header)) {
                echo'<h1>' . $header . '</h1>';
            }
            settings_errors();
            ?>
            <div id="poststuff">

                <div id="post-body" class="metabox-holder columns-<?php echo 1 == get_current_screen()->get_columns() ? '1' : '2'; ?>">

                    <div id="post-body-content">
                        <div id="cjf_base_menu_wrap" >
                            <?php
    }

    function close() {
                            ?>
                        </div>
                    </div><!-- #post-body-content -->
                    <div id="postbox-container-1" class="postbox-container">
        <?php do_meta_boxes($this->screen_id, 'side', NULL); ?>
                    </div>

                    <div id="postbox-container-2" class="postbox-container">
                        <?php do_meta_boxes($this->screen_id, 'normal', NULL); ?>
        <?php do_meta_boxes($this->screen_id, 'advanced', NULL); ?>
                    </div>
                </div> <!-- #post-body -->

            </div> <!-- #poststuff -->
        </div><!-- /.wrap -->
        <?php
    }
    
    function register_settings() {
//        // Add the settings sections so we can add our
//        // fields to it
//        add_settings_section(
//                $this->options->prefix . '_settings_section', 'Some Section', array($this, 'some_section'), $this->page_id
//        );
//
//        // Add the fields with the names and function to use for the
//        // settings, put it in their section
//        add_settings_field(
//                $this->options->prefix . '_get_url', 'Some Field', array($this, 'some_field'), $this->page_id, $this->options->prefix . '_settings_section'
//        );
//
//        // Register our setting so that $_POST handling is done for us and
//        // our callback function just has to echo the <input>
//        register_setting($this->page_id, $this->page_id, array($this, 'validate'));
    }

    function validate($values) {
        check_admin_referer( $this->page_id."_nonce" );
        return $values;
    }

    function settings_page() {
        if (!current_user_can('manage_options')) {
            wp_die('You do not have sufficient permissions to access this page.');
        }
        $this->open($this->page_headline);
        ?>
        <form method="post" action="options.php">
            <?php wp_nonce_field($this->page_id."_nonce");?>
            <?php settings_fields($this->page_id); ?>
            <?php do_settings_sections($this->page_id); ?>          
        <?php submit_button(); ?>
        </form>
        <?php
        $this->close();
    }

}
